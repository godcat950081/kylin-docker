# Kylin-Docker

## Pull docker from aliyun
```
sudo docker pull registry.cn-hangzhou.aliyuncs.com/bigdata-docker/kylin:master-0.1
sudo docker pull registry.cn-hangzhou.aliyuncs.com/bigdata-docker/kylin:slave1-0.1
sudo docker pull registry.cn-hangzhou.aliyuncs.com/bigdata-docker/kylin:slave2-0.1
sudo docker pull registry.cn-hangzhou.aliyuncs.com/bigdata-docker/kylin:mysql-0.1
```

## Run docker order by the following
```
sudo docker run -it -h master -p 7070:7070 {master}
sudo docker run -it -h slave1 {slave1}
sudo docker run -it -h slave2 {slave2}
sudo docker run --name {mysql} -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 -d {mysql:5.7}
```

## Setting hosts for all nodes
```
sudo docker exec -it {container-id} /bin/bash
### get contain's IPs by ipconfig
vim /etc/hosts
{172.17.0.2}      master
{172.17.0.3}      slave1
{172.17.0.4}      slave2
```

## Setting hive on master
```
cd /mywork/apache-hive-2.3.5-bin/conf
cp hive-default.xml.template hive-default.xml
vim hive-site.xml
  <property>
    <name>javax.jdo.option.ConnectionURL</name>
    <value>jdbc:mysql://{mysql-IP}:3306/hive?createDatabaseIfNotExist=true</value>
    <description>JDBC connect string for a JDBC metastore</description>
  </property>
```

## Initial env
```
### rm hadoop/namenode hadoop/datanode for all nodes
### rm /tmp and hadoop/tmp
### the following comment on master
hadoop namenode -format
hadoop/sbin/start-dfs.sh
hadoop/sbin/start-yarn.sh
hbase/bin/start-hbase.sh
```

## Clean (Master)
```
hbase zkcli
rmr /kylin/kylin_metadata
rmr /hbase/table/kylin_metadata
```

## Start hive (Master)
```
./hive --service metastore &
# verify hive
./hive
show databases;
quit;
```

## Start Kylin (Master)
```
{KYLIN_HONE}/bin kylin.sh start
```

## Now you can play with Kylin



# Build Kafka Cube (Option)
## Start Kafka
```
bin/kafka-server-start.sh config/server.properties
```

## Buid sample cube from kafka
```
### stop kylin
{KAFKA_HOME}/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 3 --topic kylin_streaming_topic Created topic "kylin_streaming_topic".
{KYLIN_HOME}/bin/kylin.sh org.apache.kylin.source.kafka.util.KafkaSampleProducer --topic kylin_streaming_topic --broker localhost:9092
### start kylin
{KAFKA_HOME}/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic kylin_streaming_topic --from-beginning
```

## Setup auto build
```
crontab -e
*/5 * * * * curl -X PUT --user ADMIN:KYLIN -H "Content-Type: application/json;charset=utf-8" -d '{ "sourceOffsetStart": 0, "sourceOffsetEnd": 9223372036854775807, "buildType": "BUILD"}' http://localhost:7070/kylin/api/cubes/Kafka_Cube/build2
### service cron restart
```

## Fix error if build failed at Step2
```
cp /mywork/apache-kylin-2.6.2-bin-hbase1x/conf/kylin_job_conf.xml /mywork/apache-kylin-2.6.2-bin-hbase1x/bin/meta/
cp /mywork/apache-kylin-2.6.2-bin-hbase1x/conf/kylin_hive_conf.xml /mywork/apache-kylin-2.6.2-bin-hbase1x/bin/meta/
```





