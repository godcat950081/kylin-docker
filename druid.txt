# install docker
-------------------------
docker run -it ubuntu

# update source.list in docker
sudo docker cp /etc/apt/sources.list <containerId>:/etc/apt/
apt-get update
apt-get upgrade

apt-get install wget
apt-get install net-tools
apt-get install iputils-ping
apt-get install vim
-------------------------

# install jdk
-------------------------
# download jdk 1.8 tar.gz
sudo docker cp /myworks/jdk-8u211-linux-x64.tar.gz <containerId>:/mywork/
vim ~/.bashrc
# setting for jdk
export JAVA_HOME=/mywork/jdk1.8.0_211
export JRE_HOME=${JAVA_HOME}/jre
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
export PATH=${JAVA_HOME}/bin:$PATH
source ~/.bashrc
-------------------------

# login without SSH for docker
-------------------------
apt-get install ssh
service ssh start

vim ~/.bashrc
service ssh start
source ~/.bashrc

cd ~/
ssh-keygen -t rsa -P '' -f ~/.ssh/id_dsa
cd .ssh
cat id_dsa.pub >> authorized_keys
-------------------------

# commit docker
-------------------------
exit
sudo docker commit -m "ubuntu-java1.8-withoutSSH" <containerId> ubuntu:java1.8
sudo docker tag <containerId> registry.cn-hangzhou.aliyuncs.com/bigdata-docker/ubuntu:jdk-1.8
sudo docker run -it -h apathe-druid -p 8888:8888 -p 8081:8081 -p 8090:8090 ubuntu:java1.8
**********************************************************************

sudo docker exec -it 5afde78975f7 /bin/bash

# setting hosts
-------------------------
vim /etc/hosts
172.17.0.2      master
172.17.0.3      slave1
172.17.0.4      slave2
172.17.0.5      mysql
-------------------------

# setting
-------------------------
sudo docker cp /myworks/apache-druid-0.15.0-incubating-bin.tar.gz 5afde78975f7:/mywork/
tar -zxvf apache-druid-0.15.0-incubating-bin.tar.gz
cd apache-druid-0.15.0-incubating

vim apache-druid-0.15.0-incubating/conf/druid/single-server/<micro-quickstart>/_common/common.runtime.properties
druid.zk.service.host=master
-------------------------

# start druid
-------------------------
./bin/start-micro-quickstart
# if fail about perl
apt-get install -y perl
-------------------------


# load data from file
-------------------------
sudo docker cp /myworks/DEFAULT.KYLIN_ACCOUNT.csv 5afde78975f7:/mywork/
sudo docker cp /myworks/DEFAULT.KYLIN_CAL_DT.csv 5afde78975f7:/mywork/
sudo docker cp /myworks/DEFAULT.KYLIN_CATEGORY_GROUPINGS.csv 5afde78975f7:/mywork/
sudo docker cp /myworks/DEFAULT.KYLIN_COUNTRY.csv 5afde78975f7:/mywork/
sudo docker cp /myworks/DEFAULT.KYLIN_SALES.csv 5afde78975f7:/mywork/
-------------------------


TRANS_ID,PART_DT,LSTG_FORMAT_NAME,LEAF_CATEG_ID,LSTG_SITE_ID,SLR_SEGMENT_CD,PRICE,ITEM_COUNT,SELLER_ID,BUYER_ID,OPS_USER_ID,OPS_REGION




