# pull docker images
-------------------------
sudo docker pull registry.cn-hangzhou.aliyuncs.com/bigdata-docker/bigdata_master:0.1
sudo docker pull egistry.cn-hangzhou.aliyuncs.com/bigdata-docker/bigdata_slave1:0.1
sudo docker pull registry.cn-hangzhou.aliyuncs.com/bigdata-docker/bigdata_slave2:0.1
sudo docker pull registry.cn-hangzhou.aliyuncs.com/bigdata-docker/bigdata_mysql:0.1
-------------------------

# run dockers
-------------------------
-p 7070:7070 -p 8888:8888 -p 8081:8081 -p 8090:8090 -p 2888:2888 -p 3888:3888 -p 9000:9000 -p 9001:9001 -p 2181:2181 -p 7077:7077 -p 8901:8901 -p 8079:8079 -p 1527:1527 -p 8083:8083 -p 8091:8091 -p 8082:8082 -p 8088:8088 -p 50070:50070 -p 50090:50090
sudo docker run --name bigdata_master -it -h master -p 7070:7070 -p 8888:8888 -p 8081:8081 -p 8090:8090 -p 2888:2888 -p 3888:3888 -p 9000:9000 -p 9001:9001 -p 2181:2181 -p 7077:7077 -p 8901:8901 -p 8079:8079 -p 1527:1527 -p 8083:8083 -p 8091:8091 -p 8082:8082 -p 8088:8088 -p 50070:50070 -p 50090:50090 -d bigdata_master:0.1
sudo docker run --name bigdata_slave1 -it -h slave1 -d bigdata_base:0.1
sudo docker run --name bigdata_slave2 -it -h slave2 -d bigdata_base:0.1
sudo docker run --name bigdata_druid_query -it -h druid_query -d bigdata_base:0.1
# start mysql (root/123456)
sudo docker start fabfdc050aed
-------------------------


-------------------------
# on master
docker network create --subnet=172.172.0.0/24 docker-br0
docker run --name bigdata_master -itd --network docker-br0 --ip 172.172.0.10 -h master -p 7070:7070 -p 8888:8888 -p 8081:8081 -p 8090:8090 -p 2888:2888 -p 3888:3888 -p 9000:9000 -p 9001:9001 -p 2181:2181 -p 7077:7077 -p 8901:8901 -p 8079:8079 -p 1527:1527 -p 8083:8083 -p 8091:8091 -p 8082:8082 -p 8088:8088 -p 50070:50070 -p 50090:50090 registry.cn-hangzhou.aliyuncs.com/bigdata-docker/bigdata_master:0.1 /bin/bash
docker exec -it 9e48dd68b0a2 /bin/bash
# ping 192.168.0.192
ping 172.172.1.10

# on slave2 (/home/work)
docker network create --subnet=172.172.1.0/24 docker-br0
docker run --name bigdata_slave2 -itd --network docker-br0 --ip 172.172.1.10 -h slave2 -p 7070:7070 -p 8888:8888 -p 8081:8081 -p 8090:8090 -p 2888:2888 -p 3888:3888 -p 9000:9000 -p 9001:9001 -p 2181:2181 -p 7077:7077 -p 8901:8901 -p 8079:8079 -p 1527:1527 -p 8083:8083 -p 8091:8091 -p 8082:8082 -p 8088:8088 -p 50070:50070 -p 50090:50090 -v /home/work:/mywork registry.cn-hangzhou.aliyuncs.com/bigdata-docker/bigdata_slave2:0.1 /bin/bash
docker exec -it 0850fc9e7860 /bin/bash
# ping 192.168.0.251
ping 172.172.0.10

apt-get install iproute2
master： ip route add 172.172.1.0/24 via 192.168.0.192 dev eth1
slave2： ip route add 172.172.0.0/24 via 192.168.0.251 dev eth0
-------------------------

# change hostname
-------------------------
sudo vim /etc/hostname
sudo vim /etc/sysconfig/network
HOSTNAME=new_hostname
sudo vim /etc/cloud/cloud.cfg
#- update_hostname
sudo reboot
-------------------------
